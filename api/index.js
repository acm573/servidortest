// end point de ejemplo.
var express = require('express');
var router = express.Router();


router.get('/estado', function(req, res) {

      /*
          Logica de negocio, consulta a base de datos, etc.
      */
      res.send({ codigo: '001', mensaje: 'Respuesta del servidor' });
});

router.post('/notificacion', function(req, res) {

    /*
        Logica de negocio, consulta a base de datos, etc.
    */
    console.log('El mensaje que fue enviado por LabVIEW  - ' + new Date().toLocaleDateString() + '   ' + new Date().toLocaleTimeString());
    console.log(req.body);
    console.log(' ************************************************* ')
    res.send({ codigo: '002', mensaje: 'Respuesta del servidor' });
});


module.exports = router;
