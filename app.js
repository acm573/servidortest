var config = require('./config');
var express= require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


// Usar los endpoints de la carpeta "api"
var endpoints_verifica = require('./api/index.js');
app.use('/', endpoints_verifica);

app.listen(config.puerto,function(){
       console.log("Servidor corriendo en el puerto "+config.puerto);
});
